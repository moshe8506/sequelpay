﻿namespace SequelPay.Contracts.DTO.Dashboard
{
    public class SalesVolume
    {
        public decimal AverageAmountPerDay { get; set; }
        public int AverageTransactionsPerDay { get; set; }
        public int QuantitySuccessful { get; set; }
        public int QuantityUnsuccessful { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}
