﻿namespace SequelPay.Contracts.DTO.Dashboard
{
    public class SalesVolumeWeekly : SalesVolumeDate
    {
        public int WeekNumber { get; set; }        
    }
}
