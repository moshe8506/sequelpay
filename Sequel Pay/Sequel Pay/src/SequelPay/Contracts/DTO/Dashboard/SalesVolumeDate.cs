﻿namespace SequelPay.Contracts.DTO.Dashboard
{
    using System;

    public class SalesVolumeDate : SalesVolume
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
