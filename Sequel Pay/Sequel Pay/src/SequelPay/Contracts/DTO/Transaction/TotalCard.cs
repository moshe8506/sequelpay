﻿namespace SequelPay.Contracts.DTO.Transaction
{
    using Filter;

    public class TotalCard
    {
        public TotalCard(CardType cardType, string name, int count, decimal amount)
        {
            CardType = cardType;
            Name = name;
            Count = count;
            Amount = amount;
        }

        public CardType CardType { get; }

        public string Name { get; }

        public int Count { get; }

        public decimal Amount { get; }
    }
}
