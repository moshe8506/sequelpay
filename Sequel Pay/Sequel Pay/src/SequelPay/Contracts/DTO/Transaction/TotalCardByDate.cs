﻿using System;

namespace SequelPay.Contracts.DTO.Transaction
{
    using Filter;

    public class TotalCardByDate : TotalCard
    {
        public TotalCardByDate(CardType cardType, string name, int count, decimal amount, DateTime dateCreated) : base(cardType, name, count, amount)
        {
            DateCreated = dateCreated;
        }

        public DateTime DateCreated { get; }        
    }
}
