﻿namespace SequelPay.Contracts.Filter
{
    using System;

    public class TransactionFilter : ITransactionFilter
    {
        public static readonly int LastFourCardDigintsLength = 4;

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public CardType? CardType { get; set; }
        public TransactionResults? TransactionResult { get; set; }
        public int? LastFourCardDigints { get; set; }
        public string Invoice { get; set; }
        public string CustomerName { get; set; }

        public decimal? AmountFrom { get; set; }

        public decimal? AmountTo { get; set; }

        public string SuperCardNumberFilter { get; set; }

        public string TransactionResultStringValue
        {
            get
            {
                switch (TransactionResult)
                {
                    case TransactionResults.Captured:
                    {
                        return "CAPTURED";
                    }
                    case TransactionResults.TransactionDeclined:
                    {
                        return "DECLINED";
                    }
                    case TransactionResults.Error:
                    {
                        return "ERROR";
                    }
                    default:
                    {
                        return null;
                    }
                }
            }
        }
    }
}
