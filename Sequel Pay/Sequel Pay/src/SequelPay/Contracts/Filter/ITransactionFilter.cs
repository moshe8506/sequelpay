﻿namespace SequelPay.Contracts.Filter
{
    using System;

    public interface ITransactionFilter
    {
        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }

        CardType? CardType { get; set; }

        TransactionResults? TransactionResult { get; set; }

        int? LastFourCardDigints { get; set; }
    }
}
