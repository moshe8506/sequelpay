﻿namespace SequelPay.Contracts.Filter
{
    public enum TransactionResults
    {        
        Any,
        Captured,
        TransactionDeclined,
        Error
    }
}
