﻿namespace SequelPay.Contracts.Filter
{
    public enum CardType
    {
        Any,
        Visa,
        Mastercard,
        Discover,
        Amex,
        EbtFs,
        EbtCash,
        EbtCommon
    }
}
