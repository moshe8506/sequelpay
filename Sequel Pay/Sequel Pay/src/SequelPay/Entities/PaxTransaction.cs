﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequelPay.Entities
{
    public class PaxTransaction : IEntityBase
    {
        public int Id { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.Guid> StoreID { get; set; }
        public string IpAddress { get; set; }
        public string RegisterNo { get; set; }
        public string POSVersion { get; set; }
        public string TenderType { get; set; }
        public string TransType { get; set; }
        public string ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public Nullable<int> RefNumber { get; set; }
        public string CardNumber { get; set; }
        public string NameOnCard { get; set; }
        public Nullable<decimal> RemainingBalance { get; set; }
        public Nullable<DateTime> DateCreated { get; set; }
        public string TransactionNo { get; set; }
        public string CardType { get; set; }
        public string Version { get; set; }
        public Nullable<DateTime> TranDate{ get; set; }
        public string InputMethod { get; set; }
    }
}
