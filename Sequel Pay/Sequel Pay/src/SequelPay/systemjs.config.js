﻿(function (global) {

    // map tells the System loader where to look for things
    var map = {
        'app': 'lib/spa', // 'dist',
        'rxjs': 'node_modules/rxjs',
        'angular2-in-memory-web-api': 'node_modules/angular2-in-memory-web-api',
        '@angular': 'node_modules/@angular',
        'primeng': 'node_modules/primeng',
        'ng2-bootstrap': 'node_modules/ng2-bootstrap',
        'ng2-table': 'node_modules/ng2-table',        
        'moment': 'node_modules/moment/moment.js',
        'angular2-contextmenu': 'node_modules/angular2-contextmenu'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': { main: 'app.js', defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'angular2-in-memory-web-api': { defaultExtension: 'js' },
        'primeng': { defaultExtension: 'js' },
        'ng2-bootstrap': { defaultExtension: 'js' },
        'ng2-table': { defaultExtension: 'js' },
        'angular2-contextmenu': { defaultExtension: 'js' }
    };

    var packageNames = [
      '@angular/common',
      '@angular/compiler',
      '@angular/core',
      '@angular/http',
      '@angular/platform-browser',
      '@angular/platform-browser-dynamic',
      '@angular/router',
      '@angular/router-deprecated',
      '@angular/testing',
      '@angular/upgrade'
    ];

    // add package entries for angular packages in the form '@angular/common': { main: 'index.js', defaultExtension: 'js' }
    packageNames.forEach(function (pkgName) {
        packages[pkgName] = { main: 'index.js', defaultExtension: 'js' };
    });

    var config = {
        map: map,
        packages: packages,
        paths: {
            "moment": "node_modules/moment/moment"
        }
    };

    // filterSystemConfig - index.html's chance to modify config before we register it.
    if (global.filterSystemConfig) { global.filterSystemConfig(config); }

    System.config(config);

})(this);