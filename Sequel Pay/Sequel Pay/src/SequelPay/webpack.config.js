﻿var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var DefinePlugin = require('webpack/lib/DefinePlugin');
var merge = require('webpack-merge');
var helpers = require('./conf/helpers');

var argv = require('minimist')(process.argv.slice(2));

var TARGET = argv.env;

var common = {
    //metadata: metadata,

    entry: {
        //'polyfills': './app/polyfills.ts',
        //'vendor': './wwwroot/app/vendor.ts',
        'app': './wwwroot/app/app.ts'
    },

    output: {
        path: helpers.root('wwwroot/webpackout'),
        publicPath: '',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    resolve: {
        extensions: ['', '.js', '.ts']
    },

    module: {
        preLoaders:[
            {
                test: /\.js$/,
                loader: 'source-map-loader',
                exclude: [
                    // these packages have problems with their sourcemaps
                    helpers.root("./node_modules/rxjs")                   
                ]
            }
        ],
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts'
            },
            {
                test: /\.html$/,
                loader: 'raw'
                //exclude: [helpers.root('./wwwroot/app/app.html')]
            },
            {                
                test: /\.(gif|png|jpe?g|ico|woff|woff2|eot|ttf|svg|swf|otf)(\?.*$|$)/,
                loader: 'file?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.css$/,
                exclude: helpers.root('wwwroot/app'),
                loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
            },
            {
                test: /\.css$/,
                include: helpers.root('wwwroot/app'),
                loader: 'raw'
            }
        ]
    },

    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app']
        }),
        new CopyWebpackPlugin([
            {
                from: './wwwroot/app/images',
                to: 'images'
            }
        ]),
        new HtmlWebpackPlugin({
            template: './wwwroot/app/app.html'
        }),
        new ExtractTextPlugin('[name].css'),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
        //new DefinePlugin({
        //    "NODE_ENV": JSON.stringify(metadata.NODE_ENV),
        //    "API_URL": JSON.stringify(metadata.API_URL),
        //    "BASE_HREF": JSON.stringify(metadata.BASE_HREF)
        //})
    ],

    devtool: "source-map"
};

var dev = {
    devServer: {
        historyApiFallback: true,
        inline: true,
        outputPath: helpers.root('wwwroot')
    }
};

var config = common;
if (TARGET === "dev") {
    config = merge(config, dev);
}

module.exports = config;