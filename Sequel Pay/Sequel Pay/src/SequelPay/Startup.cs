﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SequelPay.Infrastructure;
using Microsoft.EntityFrameworkCore;
using SequelPay.Infrastructure.Repositories;
using SequelPay.Infrastructure.Mappings;
using System.Security.Claims;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SequelPay.Infrastructure.Services;

namespace SequelPay
{
    using Infrastructure.Repositories.Abstract;
    using Newtonsoft.Json.Serialization;
    using Services.Common;
    using Services.Transaction;

    public class Startup
    {
        private static string _applicationPath = string.Empty;
        private static string _contentRootPath = string.Empty;
        public Startup(IHostingEnvironment env)
        {
            _applicationPath = env.WebRootPath;
            _contentRootPath = env.ContentRootPath;
            // Setup configuration sources.

            var builder = new ConfigurationBuilder()
                .SetBasePath(_contentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // This reads the configuration keys from the secret store.
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();
            }
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
         /*   services.AddAuthorization(options => {
                options.AddPolicy("AdministratorOnly", policy => policy.RequireRole("Administrator"));
                options.AddPolicy("Authenticated", policy => policy.RequireAuthenticatedUser());
            });
		 */
            services.AddDbContext<SequelPayContext>(options =>
                options.UseSqlServer(Configuration["Data:SequelPayConnection:ConnectionString"]));


            // Repositories
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ILoggingRepository, LoggingRepository>();
            services.AddScoped<ITransactionsRepository, TransactionsRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();

     

            // Services
			services.AddScoped<IMembershipService, MembershipService>();
            //services.AddScoped<UserManager<ApplicationUser>, UserManager<ApplicationUser>>();
            //   services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<ICardTypeService, CardTypeService>();

            services.AddAuthentication();

            services.Configure<IdentityOptions>(options =>
            {
          
                options.Cookies.ApplicationCookie.AutomaticAuthenticate = true;
                options.Cookies.ApplicationCookie.AutomaticChallenge = false;
                // options.Cookies.ApplicationCookie.LoginPath = PathString.Empty;
            
                // new Microsoft.AspNetCore.Http.PathString("/#/account");
            });

            // Polices
            services.AddAuthorization(options =>
             {
               
                 // inline policies
                 options.AddPolicy("AdministratorOnly", policy =>
                 {
                     policy.RequireClaim(ClaimTypes.Role, "Admin");
                 });
                 options.AddPolicy("MerchantOnly", policy =>
                 {
                     policy.RequireClaim(ClaimTypes.Role, "Merchant");
                 });
             });

            services.AddIdentity<Models.ApplicationUser, IdentityRole>(o =>
            {
               
              //  o.SignIn.RequireConfirmedEmail
                o.Password.RequireDigit = false;
                o.Password.RequiredLength = 5;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireLowercase = false;
            }).AddEntityFrameworkStores<SequelPayContext>()
                .AddDefaultTokenProviders();

            // Add MVC services to the services container.
            services.AddMvc().AddJsonOptions(opt =>
            {
                opt.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;

                var resolver = opt.SerializerSettings.ContractResolver as DefaultContractResolver;
                if (resolver != null)
                {
                    resolver.NamingStrategy = null; // this removes the camelcasing
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
       
            // this will serve up wwwroot
            app.UseFileServer();

            // this will serve up node_modules
            var provider = new PhysicalFileProvider(
                Path.Combine(_contentRootPath, "node_modules")
            );
            var _fileServerOptions = new FileServerOptions();
            _fileServerOptions.RequestPath = "/node_modules";
            _fileServerOptions.StaticFileOptions.FileProvider = provider;
            _fileServerOptions.EnableDirectoryBrowsing = true;
            app.UseFileServer(_fileServerOptions);

            AutoMapperConfiguration.Configure();

            app.UseIdentity();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
               AuthenticationScheme = "Cookie",
            //    LoginPath = new PathString("/#/account"),
            //    LogoutPath = new PathString("/#/logout"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = false
      
            //    //AccessDeniedPath = new PathString("/Account/Forbidden/"),
            //    //AutomaticAuthenticate = true,
            //    //AutomaticChallenge = true

        });

            // Custom authentication middleware
            //app.UseMiddleware<AuthMiddleware>();

            app.UseDeveloperExceptionPage();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute("spa-fallback",
                    "{*anything}",
                    new {controller = "Home", action = "Index"});

            });

            // DbInitializer.Initialize(app.ApplicationServices, _applicationPath);
        }

        // Entry point for the application.
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
               .UseUrls("http://0.0.0.0:5000/")
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .Build();

            host.Run();
        }
    }
}
