﻿namespace SequelPay.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Contracts.Filter;
    using Microsoft.AspNetCore.Mvc;
    using Entities;
    using Infrastructure.Repositories;
    using Infrastructure.Core;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Models;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Services.Transaction;
    using Microsoft.EntityFrameworkCore;

    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly ITransactionService _transactionService;
        readonly ILoggingRepository _loggingRepository;
        readonly RoleManager<IdentityRole> _roleManager;
        readonly UserManager<ApplicationUser> _userManager;

        public TransactionsController(IAuthorizationService authorizationService,
                                ITransactionService transactionService,
                                ILoggingRepository loggingRepository,
                                RoleManager<IdentityRole> roleManager,
                                UserManager<ApplicationUser> userManager
                              )
        {
            _transactionService = transactionService;
            _authorizationService = authorizationService;
            _loggingRepository = loggingRepository;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [Authorize(Policy = "MerchantOnly")]
        [HttpGet("{page:int=0}/{pageSize=10}")]
        public async Task<IActionResult> Get(int? page, int? pageSize, [FromQuery] TransactionFilter transactionFilter)
        {
            //  CreateUserAndRoles
            PaginationSet<PaxTransaction> pagedSet = null;
            try
            {
                if (await _authorizationService.AuthorizeAsync(User, "MerchantOnly"))
                {
                    var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;
                    var pax = _transactionService.GetTransactions(storeId, transactionFilter);

                    //todo multiple queries
                    pagedSet = new TransactionSet()
                    {
                        TotalCount = pax.Count(),
                        Items = await pax.Skip(page ?? 0 * pageSize ?? 0).Take(pageSize ?? 0).ToListAsync(),
                        // todo group by by Any card
                        TotalCards = await _transactionService.GetTotalCards(storeId, transactionFilter),
                        TotalByAllCards = pax.Sum(p => p.Amount ?? 0)
                    };

                    return new ObjectResult(pagedSet);
                }

                //todo constant                
                return new ObjectResult(new CodeResultStatus(401));
            }
            catch (Exception ex)
            {
                _loggingRepository.Add(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                });
                _loggingRepository.Commit();
            }

            return new ObjectResult(pagedSet);
        }


        [Authorize(Policy = "MerchantOnly")]
        [HttpGet("totals")]
        public async Task<IActionResult> GetTotals([FromQuery] TransactionFilter transactionFilter)
        {
            //// todo for test only!
            //transactionFilter.StartDate = transactionFilter.StartDate.Value.Subtract(TimeSpan.FromDays(12));
            //transactionFilter.EndDate = transactionFilter.EndDate.Value.Subtract(TimeSpan.FromDays(12));

            var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;
            var cards = await _transactionService.GetTotalCards(storeId, transactionFilter);
            var cardsWithoutAny = cards.Where(c => c.CardType != CardType.Any).ToList();
            var totalAmount = cardsWithoutAny.Sum(c => c.Amount);
            var totalCount = cardsWithoutAny.Sum(c => c.Count);
            return new ObjectResult(new {cards, totalAmount, totalCount});
        }

        //todo depricated
        //// todo move to populate
        //private async void CreateUserAndRoles()
        //{
        //    // In Startup iam creating first Admin Role and creating a default Admin User     
        //    if (await _roleManager.RoleExistsAsync("Admin") == false)
        //    {
        //        // first we create Admin rool    
        //        var role = new IdentityRole();
        //        role.Name = "Merchant";
        //        await _roleManager.CreateAsync(role);

        //    }
        //    var user = new ApplicationUser();
        //    user.UserName = "GlattMart";
        //    user.Email = "eziel8050@gmail.com";
        //    user.StoreID = Guid.Parse("2F30DB61-7BCF-4C87-823B-BC6CFCED26E9");
        //    string userPWD = "glat789";

        //    var chkUser = await _userManager.CreateAsync(user, userPWD);

        //    //Add default User to Role Admin    
        //    if (chkUser.Succeeded)
        //    {
        //        //  var result1 = await _userManager.AddToRoleAsync(user, "Admin");
        //        var result2 = await _userManager.AddToRoleAsync(user, "Merchant");
        //    }

        //}

        public IActionResult Transactions() => PartialView();
        public IActionResult Index()
        {
            return View();
        }

    }
}
