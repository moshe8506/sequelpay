﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SequelPay.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using SequelPay.Infrastructure.Services;
using SequelPay.Infrastructure.Repositories;
using SequelPay.Entities;
using System.Security.Claims;
using SequelPay.Infrastructure;
using SequelPay.Infrastructure.Core;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using SequelPay.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SequelPay.Controllers
{
    using System.Linq;
    using Infrastructure.Repositories.Abstract;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IMembershipService _membershipService;
        private readonly IUserRepository _userRepository;
        private readonly ILoggingRepository _loggingRepository;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUsersRepository _usersRepository;

        public AccountController(IMembershipService membershipService,
            IUserRepository userRepository,
            ILoggingRepository _errorRepository,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IUsersRepository usersRepository)
        {
            _membershipService = membershipService;
            _userRepository = userRepository;
            _loggingRepository = _errorRepository;
            _signInManager = signInManager;
            _userManager = userManager;
            _usersRepository = usersRepository;
        }


        [HttpPost("authenticate")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel user, string returnUrl = null)
        {
            IActionResult _result = new ObjectResult(false);
            GenericResult _authenticationResult = null;
            try
            {
                ViewData["ReturnUrl"] = returnUrl;
                if (ModelState.IsValid)
                {
                    var result = await _signInManager.PasswordSignInAsync(user.Username, user.Password, user.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        _authenticationResult = new GenericResult()
                        {
                            Succeeded = true,
                            Message = "Authentication succeeded"
                        };
                    }
                    else
                    {
                        _authenticationResult = new GenericResult()
                        {
                            Succeeded = false,
                            Message = "Authentication failed" 
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                _authenticationResult = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };

                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();
            }
            _result = new ObjectResult(_authenticationResult);
            return _result;
        }
        
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                /*    if (_signInManager.IsSignedIn(User))
                   {
                       await _signInManager.SignOutAsync();

                      if (!_signInManager.IsSignedIn(User))
                       { return Ok(); }
                       else
                         return BadRequest();
                   }
                   else { return Ok(); }*/
                await _signInManager.SignOutAsync();
                return Ok();

            }
            catch (Exception ex)
            {
                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();

                return BadRequest();
            }

        }

        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel user)
        {           
            GenericResult registrationResult;
            try
            {
                if (ModelState.IsValid)
                {
                    Guid guid;
                    // todo validation in model
                    if (!Guid.TryParse(user.StoreId, out guid))
                    {
                        return new ObjectResult(new GenericResult()
                        {
                            Succeeded = false,
                            Message = $"Invalid guid format for {nameof(user.StoreId)}"
                        });
                    }

                    var applicationUser = new ApplicationUser()
                    {
                        UserName = user.Username,
                        Email = user.Email,
                        StoreID = guid,
                        // Create user with Merchant role is ok for now
                        Claims =
                        {
                            new IdentityUserClaim<string>()
                            {
                                ClaimType = ClaimTypes.Role,
                                ClaimValue = "Merchant"
                            }
                        }
                    };

                    var identityResult = await _userManager.CreateAsync(applicationUser, user.Password);
                    registrationResult = new GenericResult()
                    {
                        Succeeded = identityResult.Succeeded,
                        Message =
                            identityResult.Succeeded
                                ? "Registration succeeded"
                                : string.Join(",", identityResult.Errors.Select(e => e.Description))
                    };
                }
                else
                {
                    registrationResult = new GenericResult()
                    {
                        Succeeded = false,
                        Message = "Invalid fields."
                    };
                }
            }
            catch (Exception ex)
            {
                registrationResult = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };

                _loggingRepository.Add(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                });
                _loggingRepository.Commit();
            }            
            return new ObjectResult(registrationResult); ;
        }

        // todo simple implementation get users
        [Authorize]
        [Route("users")]
        [HttpGet("{page:int=0}/{pageSize=10}")]
        public IActionResult GetUsers(int page, int pageSize)
        {
            // TODO move to service
            var users = _usersRepository.GetUsers().ToList();
            var dtoResult = users.Select(u => new
            {
                u.NormalizedUserName,
                u.NormalizedEmail,
                StoreID = u.StoreID.ToString()
            }).ToList();
            return new ObjectResult(dtoResult);
        }
    }
}
