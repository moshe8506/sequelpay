﻿namespace SequelPay.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Threading.Tasks;
    using Contracts.Filter;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Services.Transaction;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    [Route("api/[controller]")]
    public class ChartController : Controller
    {
        readonly UserManager<ApplicationUser> _userManager;
        private readonly ITransactionService _transactionService;

        public ChartController(ITransactionService transactionService, UserManager<ApplicationUser> userManager)
        {
            _transactionService = transactionService;
            _userManager = userManager;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] TransactionFilter transactionFilter)
        {            
            var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;            
            var transactionsByCardByDate = _transactionService.GetTotalCardsByDay(storeId, transactionFilter);

            var resultList = new List<object>();            
            foreach (var kvp in await transactionsByCardByDate)
            {
                dynamic expandoObject = new ExpandoObject();
                var result = (IDictionary<string, object>)expandoObject;
                result["DateCreated"] = kvp.Key;
                foreach (var totalCardByDate in kvp.Value)
                {                    
                    result[totalCardByDate.CardType.ToString()] = totalCardByDate.Amount;
                }
                resultList.Add(result);
            }

            return new ObjectResult(resultList);
        }

        [HttpGet("SalesVolume")]
        public async Task<IActionResult> GetSalesVolume([FromQuery] TransactionFilter transactionFilter)
        {
            var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;            
            var salesVolume = await _transactionService.GetSalesVolume(storeId, transactionFilter);
            return new ObjectResult(salesVolume);
        }

        [HttpGet("SalesVolumeWeekly")]
        public async Task<IActionResult> GetSalesVolumeWeekly([FromQuery] TransactionFilter transactionFilter)
        {
            var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;
            var salesVolumeWeekly = await _transactionService.GetSalesVolumeWeekly(storeId, transactionFilter);
            return new ObjectResult(salesVolumeWeekly);
        }

        [HttpGet("SalesVolumeDaily/{page:int=0}/{pageSize?}")]
        public async Task<IActionResult> SalesVolumeDaily(int page, int? pageSize, [FromQuery] TransactionFilter transactionFilter)
        {
            var storeId = _userManager.FindByNameAsync(User.Identity.Name).Result.StoreID;
            var salesVolumeDaily = _transactionService.GetSalesVolumeDaily(storeId, transactionFilter);

            if (pageSize.HasValue)
            {
                salesVolumeDaily = salesVolumeDaily.Skip(page*pageSize.Value).Take(pageSize.Value);
            }

            return new ObjectResult(await salesVolumeDaily.ToListAsync());
        }

    }
}
