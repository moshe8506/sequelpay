﻿namespace SequelPay.Services.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.Filter;

    public class CardTypeService : ICardTypeService
    {
        private readonly IDictionary<CardType, string> _cardTypeDictionary;

        public CardTypeService()
        {
            _cardTypeDictionary = new Dictionary<CardType, string>
            {
                {
                    CardType.Visa, 
                    "VISA"
                },
                {
                    CardType.Mastercard,
                    "MASTERCARD"
                },
                {
                    CardType.Discover,
                    "DISCOVER"
                },
                {
                    CardType.Amex,
                    "AMEX"
                },
                {
                    CardType.EbtCash,
                    "EBT Cash"
                },
                {
                    CardType.EbtFs,
                    "EBT FS"
                },
                // not present in DB
                {
                    CardType.EbtCommon, 
                    "EBT"
                }
            };
        }

        public string GetNameByCardType(CardType cardType)
        {
            return _cardTypeDictionary.ContainsKey(cardType) ? _cardTypeDictionary[cardType] : string.Empty;
        }

        public CardType GetCardTypeByName(string name)
        {
            return _cardTypeDictionary.Values.Contains(name, StringComparer.CurrentCultureIgnoreCase)
                ? _cardTypeDictionary.First(kvp => kvp.Value.Equals(name, StringComparison.CurrentCultureIgnoreCase)).Key
                : CardType.Any;
        }
    }
}
