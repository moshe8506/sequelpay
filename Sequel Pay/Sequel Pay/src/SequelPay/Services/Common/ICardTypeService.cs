﻿namespace SequelPay.Services.Common
{
    using Contracts.Filter;    

    public interface ICardTypeService
    {
        string GetNameByCardType(CardType cardType);

        CardType GetCardTypeByName(string name);
    }
}
