﻿namespace SequelPay.Services.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Contracts.DTO.Dashboard;
    using Contracts.DTO.Transaction;
    using Contracts.Filter;
    using Entities;

    public interface ITransactionService
    {
        IQueryable<PaxTransaction> GetTransactions(Guid storeId, TransactionFilter filter);

        Task<ICollection<TotalCard>> GetTotalCards(Guid storeId, TransactionFilter transactionFilter);

        Task<IDictionary<DateTime, IEnumerable<TotalCardByDate>>> GetTotalCardsByDay(Guid storeId, TransactionFilter transactionFilter);

        Task<SalesVolume> GetSalesVolume(Guid storeId, TransactionFilter transactionFilter);

        Task<IEnumerable<SalesVolumeWeekly>> GetSalesVolumeWeekly(Guid storeId, TransactionFilter transactionFilter);

        IQueryable<SalesVolumeDate> GetSalesVolumeDaily(Guid storeId, TransactionFilter transactionFilter);
    }
}
