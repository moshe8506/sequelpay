﻿namespace SequelPay.Services.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using Common;
    using Contracts.DTO.Dashboard;
    using Contracts.DTO.Transaction;
    using Contracts.Filter;
    using Entities;    
    using Infrastructure.Repositories;
    using Microsoft.EntityFrameworkCore;

    public class TransactionService : ITransactionService
    {
        private readonly ITransactionsRepository _transRepository;
        private readonly ICardTypeService _cardTypeService;

        public TransactionService(ITransactionsRepository transactionsRepository, ICardTypeService cardTypeService)
        {
            _transRepository = transactionsRepository;
            _cardTypeService = cardTypeService;
        }

        public IQueryable<PaxTransaction> GetTransactions(Guid storeId, TransactionFilter transactionFilter)
        {
            IQueryable<PaxTransaction> pax = _transRepository.GetAllQueryable()
                        .Where(p => p.StoreID == storeId)
                        .OrderByDescending(a => a.DateCreated);

            if (!string.IsNullOrWhiteSpace(transactionFilter.SuperCardNumberFilter))
            {
                pax =
                    pax.Where(
                        p =>
                            !string.IsNullOrWhiteSpace(p.CardNumber) &&
                            p.CardNumber.Trim()
                                .Equals(transactionFilter.SuperCardNumberFilter,
                                    StringComparison.CurrentCultureIgnoreCase));
                return pax;
            }

            pax =
                pax.Where(
                    p => p.DateCreated.HasValue &&
                         p.DateCreated.Value.Date >= transactionFilter.StartDate.Value.Date &&
                         p.DateCreated.Value.Date <= transactionFilter.EndDate.Value.Date);

            if (transactionFilter.CardType.HasValue && transactionFilter.CardType.Value != CardType.Any)
            {
                pax =pax.Where(p => !string.IsNullOrWhiteSpace(p.CardType));

                if (transactionFilter.CardType.Value == CardType.EbtCommon)
                {
                    pax =
                        pax.Where(
                            p =>
                                p.CardType.Equals(_cardTypeService.GetNameByCardType(CardType.EbtCash),
                                    StringComparison.CurrentCultureIgnoreCase)
                                ||
                                p.CardType.Equals(_cardTypeService.GetNameByCardType(CardType.EbtFs),
                                    StringComparison.CurrentCultureIgnoreCase)
                            );
                }
                else
                {
                    pax =
                        pax.Where(
                            p =>
                                p.CardType.Equals(_cardTypeService.GetNameByCardType(transactionFilter.CardType.Value),
                                    StringComparison.CurrentCultureIgnoreCase));
                }
            }

            // TODO create a separate dictionary for transaction results
            if (transactionFilter.TransactionResult.HasValue && transactionFilter.TransactionResult.Value != TransactionResults.Any)
            {
                pax =
                    pax.Where(
                        p => !string.IsNullOrWhiteSpace(p.ResultCode) &&
                             p.ResultCode.Equals(transactionFilter.TransactionResultStringValue.ToString(),
                                 StringComparison.CurrentCultureIgnoreCase));
            }

            if (transactionFilter.LastFourCardDigints.HasValue &&
                transactionFilter.LastFourCardDigints.Value.ToString().Length ==
                TransactionFilter.LastFourCardDigintsLength)
            {
                pax = pax.Where(p => !string.IsNullOrWhiteSpace(p.CardNumber) &&
                                     p.CardNumber.Trim().EndsWith(transactionFilter.LastFourCardDigints.ToString()));
            }

            if (!string.IsNullOrWhiteSpace(transactionFilter.Invoice))
            {
                pax = pax.Where(p => !string.IsNullOrWhiteSpace(p.TransactionNo) &&
                                     p.TransactionNo.Equals(transactionFilter.Invoice,
                                         StringComparison.CurrentCultureIgnoreCase));
            }
            if (!string.IsNullOrWhiteSpace(transactionFilter.CustomerName))
            {
                pax = pax.Where(p => !string.IsNullOrWhiteSpace(p.NameOnCard) &&
                                     p.NameOnCard.Trim().ToUpper().Contains(transactionFilter.CustomerName.ToUpper()));
            }

            if (transactionFilter.AmountFrom.HasValue)
            {
                pax = pax.Where(p => p.Amount.HasValue && p.Amount.Value >= transactionFilter.AmountFrom.Value);
            }

            if (transactionFilter.AmountTo.HasValue)
            {
                pax = pax.Where(p => p.Amount.HasValue && p.Amount.Value <= transactionFilter.AmountTo.Value);
            }

            return pax;
        }

        public async Task<ICollection<TotalCard>> GetTotalCards(Guid storeId, TransactionFilter transactionFilter)
        {
            var transactions = GetTransactions(storeId, transactionFilter);
            var totalCards = await transactions.GroupBy(t => t.CardType)
                .Select(
                    g =>
                        new TotalCard(_cardTypeService.GetCardTypeByName(g.Key), g.Key, g.Count(),
                            g.Sum(t => t.Amount ?? 0)))
                .ToListAsync();

            return totalCards.GroupBy(
                tc =>
                    new
                    {
                        CardType =
                            tc.CardType == CardType.EbtCash || tc.CardType == CardType.EbtFs
                                ? CardType.EbtCommon
                                : tc.CardType
                    })
                .Select(
                    g =>
                        new TotalCard(g.Key.CardType, _cardTypeService.GetNameByCardType(g.Key.CardType),
                            g.Sum(tc => tc.Count),
                            g.Sum(t => t.Amount)))
                .ToList();
        }

        public async Task<IDictionary<DateTime, IEnumerable<TotalCardByDate>>> GetTotalCardsByDay(Guid storeId, TransactionFilter transactionFilter)
        {
            var transactions = GetTransactions(storeId, transactionFilter);
            var totalCards = await transactions
                .Where(t => t.DateCreated.HasValue)
                .GroupBy(
                    t =>
                        new
                        {
                            t.CardType,
                            DateCreated = t.DateCreated.Value.Date
                        })
                .Select(
                    g =>
                        new TotalCardByDate(_cardTypeService.GetCardTypeByName(g.Key.CardType), g.Key.CardType,
                            g.Count(),
                            g.Sum(t => t.Amount ?? 0), g.Key.DateCreated))
                .ToListAsync();

            totalCards =
                totalCards.GroupBy(
                    tc =>
                        new
                        {
                            CardType =
                                tc.CardType == CardType.EbtCash || tc.CardType == CardType.EbtFs
                                    ? CardType.EbtCommon
                                    : tc.CardType,
                            tc.DateCreated
                        })
                    .Select(
                        g =>
                            new TotalCardByDate(g.Key.CardType, _cardTypeService.GetNameByCardType(g.Key.CardType),
                                g.Sum(tc => tc.Count),
                                g.Sum(t => t.Amount), g.Key.DateCreated))
                    .ToList();

            return totalCards.GroupBy(t => t.DateCreated)
                .ToDictionary(g => g.Key, g => g as IEnumerable<TotalCardByDate>);
        }

        // todo remove hard coded "CAPTURED"
        public async Task<SalesVolume> GetSalesVolume(Guid storeId, TransactionFilter transactionFilter)
        {
            var transactions = GetTransactions(storeId, transactionFilter);
            var cardAverage = await transactions
                .Where(t => t.DateCreated.HasValue)
                .GroupBy(t => t.DateCreated.Value.Date)
                .Select(g => new {GroupTransactionCount = g.Count(), GroupTransactionAmount = g.Sum(t => t.Amount ?? 0)})
                .ToListAsync();

            var amount = await transactions.Where(t => t.Amount.HasValue).SumAsync(t => t.Amount.Value);
            var quantity = await transactions.CountAsync();
            var averageTransactionsPerDay = (int) cardAverage.Average(g => g.GroupTransactionCount);
            var averageAmountPerDay = cardAverage.Average(g => g.GroupTransactionAmount);
            var quantitySuccessful =
                await
                    transactions.Where(
                        t =>
                            !string.IsNullOrWhiteSpace(t.ResultCode) &&
                            t.ResultCode.Equals("CAPTURED", StringComparison.CurrentCultureIgnoreCase)).CountAsync();
            var quantityUnsuccessful =
                await
                    transactions.Where(
                        t =>
                            string.IsNullOrWhiteSpace(t.ResultCode) ||
                            !t.ResultCode.Equals("CAPTURED", StringComparison.CurrentCultureIgnoreCase)).CountAsync();

            return new SalesVolume
            {
                Amount = amount,
                Quantity = quantity,
                AverageTransactionsPerDay = averageTransactionsPerDay,
                AverageAmountPerDay = averageAmountPerDay,
                QuantitySuccessful = quantitySuccessful,
                QuantityUnsuccessful = quantityUnsuccessful
            };
        }

        public async Task<IEnumerable<SalesVolumeWeekly>> GetSalesVolumeWeekly(Guid storeId, TransactionFilter transactionFilter)
        {
            var transactions = GetTransactions(storeId, transactionFilter);
            Func<DateTime, int> weekProjector =
                d => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                    d,
                    CalendarWeekRule.FirstFourDayWeek,
                    DayOfWeek.Sunday);

            return await transactions
                .Where(t => t.DateCreated.HasValue)
                .GroupBy(t => weekProjector(t.DateCreated.Value))
                .Select(g => new SalesVolumeWeekly
                {
                    WeekNumber = g.Key,
                    Amount = g.Sum(t => t.Amount ?? 0),
                    Quantity = g.Count()
                })
                .ToListAsync();
        }

        public IQueryable<SalesVolumeDate> GetSalesVolumeDaily(Guid storeId, TransactionFilter transactionFilter)
        {
            var transactions = GetTransactions(storeId, transactionFilter);
            return transactions
                .Where(t => t.DateCreated.HasValue)
                .GroupBy(t => t.DateCreated.Value.Date)
                .Select(g => new SalesVolumeDate
                {                    
                    StartDate = g.Key,
                    EndDate = g.Key,
                    Amount = g.Sum(t => t.Amount ?? 0),
                    Quantity = g.Count()
                });
        }
    }
}
