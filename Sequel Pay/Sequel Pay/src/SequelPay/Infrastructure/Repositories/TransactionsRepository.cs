﻿using SequelPay.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SequelPay.Infrastructure.Repositories
{
    public class TransactionsRepository : EntityBaseRepository<PaxTransaction>, ITransactionsRepository
    {
        public TransactionsRepository(SequelPayContext context)
            : base(context)
        { }
    }
}