﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SequelPay.Infrastructure.Repositories
{
    using Abstract;
    using Models;

    public class UsersRepository : IUsersRepository
    {
        private readonly SequelPayContext _context;

        public UsersRepository(SequelPayContext context)
        {
            _context = context;
        }

        public IEnumerable<ApplicationUser> GetUsers()
        {
            return _context.Users;
        }
    }
}
