﻿using SequelPay.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SequelPay.Infrastructure.Repositories
{

    public interface ITransactionsRepository : IEntityBaseRepository<PaxTransaction> { }
	
    public interface ILoggingRepository : IEntityBaseRepository<Error> { }


    public interface IRoleRepository : IEntityBaseRepository<Role> { }

    public interface IUserRepository : IEntityBaseRepository<User>
    {
        User GetSingleByUsername(string username);
        IEnumerable<Role> GetUserRoles(string username);
    }

    public interface IUserRoleRepository : IEntityBaseRepository<UserRole> { }
}
