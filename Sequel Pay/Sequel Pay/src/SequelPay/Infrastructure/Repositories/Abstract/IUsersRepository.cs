﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SequelPay.Infrastructure.Repositories.Abstract
{
    using Models;
    public interface IUsersRepository
    {
        IEnumerable<ApplicationUser> GetUsers();
    }
}
