﻿using Microsoft.EntityFrameworkCore;
using SequelPay.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SequelPay.Models;
using Microsoft.AspNetCore.Identity;

namespace SequelPay.Infrastructure
{
    public  class DbInitializer : Microsoft.AspNetCore.Mvc.Controller
    {
        private static RoleManager<IdentityRole> _roleManager;

        public DbInitializer(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }

        private static SequelPayContext context;

        public static void Initialize(IServiceProvider serviceProvider, string imagesPath)
        {
            context = (SequelPayContext)serviceProvider.GetService(typeof(SequelPayContext));
            createRolesandUsers();
        }
        private static async void createRolesandUsers()
        {

            // In Startup iam creating first Admin Role and creating a default Admin User     
            if (await _roleManager.RoleExistsAsync("Admin") == false)
            {
             // first we create Admin rool    
                var role = new IdentityRole();
                role.Name = "Admin";
                await _roleManager.CreateAsync(role);

            }
                      
        }
    }
}
