﻿using Microsoft.EntityFrameworkCore;
using SequelPay.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SequelPay.Models;

namespace SequelPay.Infrastructure
{
    public class SequelPayContext : IdentityDbContext<ApplicationUser>
    {
   
       // public DbSet<User> Users { get; set; }
      //   public DbSet<Role> Roles { get; set; }
      //  public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<PaxTransaction> PaxTransaction { get; set;}

        public SequelPayContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
