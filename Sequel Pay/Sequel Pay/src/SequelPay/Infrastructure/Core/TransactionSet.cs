﻿namespace SequelPay.Infrastructure.Core
{
    using System.Collections.Generic;
    using Contracts.DTO.Transaction;
    using Entities;    

    public class TransactionSet : PaginationSet<PaxTransaction>
    {
        public ICollection<TotalCard> TotalCards { get; set; }

        public decimal TotalByAllCards { get; set; }        
    }
}
