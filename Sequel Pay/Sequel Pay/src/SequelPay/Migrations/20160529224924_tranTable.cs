﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SequelPay.Migrations
{
    public partial class tranTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaxTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CardNumber = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IpAddress = table.Column<string>(nullable: true),
                    NameOnCard = table.Column<string>(nullable: true),
                    RefNumber = table.Column<int>(nullable: false),
                    RegisterNo = table.Column<string>(nullable: true),
                    RemainingBalance = table.Column<decimal>(nullable: false),
                    ResultCode = table.Column<string>(nullable: true),
                    ResultMessage = table.Column<string>(nullable: true),
                    StoreID = table.Column<Guid>(nullable: false),
                    TenderType = table.Column<string>(nullable: true),
                    TransType = table.Column<string>(nullable: true),
                    TransactionID = table.Column<int>(nullable: false),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaxTransaction", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaxTransaction");
        }
    }
}
