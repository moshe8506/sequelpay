﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SequelPay.Migrations
{
    public partial class StoreID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
       
            migrationBuilder.AddColumn<Guid>(
                name: "StoreID",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
            }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      
            migrationBuilder.DropColumn(
                name: "StoreID",
                table: "AspNetUsers");

      
        }
    }
}
