﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SequelPay.Migrations
{
    public partial class fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      

            migrationBuilder.AlterColumn<Guid>(
                name: "StoreID",
                table: "PaxTransaction",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RemainingBalance",
                table: "PaxTransaction",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RefNumber",
                table: "PaxTransaction",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "PaxTransaction",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "PaxTransaction",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "StoreID",
                table: "PaxTransaction",
                nullable: false);

            migrationBuilder.AlterColumn<decimal>(
                name: "RemainingBalance",
                table: "PaxTransaction",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "RefNumber",
                table: "PaxTransaction",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "PaxTransaction",
                nullable: false);

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "PaxTransaction",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "TransactionID",
                table: "PaxTransaction",
                nullable: false,
                defaultValue: 0);
        }
    }
}
