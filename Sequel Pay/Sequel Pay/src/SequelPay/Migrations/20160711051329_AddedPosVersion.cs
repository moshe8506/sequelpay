﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SequelPay.Migrations
{
    public partial class AddedPosVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "POSVersion",
                table: "PaxTransaction",
                nullable: true);
         }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CardType",
                table: "PaxTransaction");

            migrationBuilder.DropColumn(
                name: "POSVersion",
                table: "PaxTransaction");

            migrationBuilder.AddColumn<string>(
                name: "TranType",
                table: "PaxTransaction",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName");
        }
    }
}
