﻿export  interface IEnumEntity {
    name: string;
    value: number;
}