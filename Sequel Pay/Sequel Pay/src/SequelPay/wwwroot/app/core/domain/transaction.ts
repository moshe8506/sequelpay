﻿export class Transactions {
    Id: number;
    Title: string;
    CardNumber: string;
    Thumbnail: string;
    DateCreated: Date;
    TotalPhotos: number;

    constructor(id: number,
        title: string,
        cardNumber: string,
        thumbnail: string,
        dateCreated: Date,
        totalPhotos: number) {
        this.Id = id;
        this.Title = title;
        this.CardNumber = cardNumber;
        this.Thumbnail = thumbnail;
        this.DateCreated = dateCreated;
        this.TotalPhotos = totalPhotos;
    }
}