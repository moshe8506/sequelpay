﻿export class SalesVolume {
    AverageAmountPerDay: number;
    AvarageTransactionsPerDay: number;
    QuantitySuccessful: number;
    QuantityUnsuccessful: number;
    Quantity: number;
    Amount: number;
}