﻿export class UserFromList {
    public userName: string;
    public email: string;
    public storeId: string;

    public constructor(userName : string, email: string, storeId: string) {
        this.userName = userName;
        this.email = email;
        this.storeId = storeId;
    }
}