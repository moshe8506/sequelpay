﻿import { CardTypes } from './cardTypes';

// todo to lower case
export interface ICardTotal {
    CardType: CardTypes;
    Name : string;
    Count: number;
    Amount: number;

}