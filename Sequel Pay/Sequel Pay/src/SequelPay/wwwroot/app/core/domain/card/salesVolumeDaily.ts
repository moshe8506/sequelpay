﻿import { SalesVolume } from './salesVolume';

export class SalesVolumeDaily extends SalesVolume {
    StartDate: Date;
    EndDate: Date;
}