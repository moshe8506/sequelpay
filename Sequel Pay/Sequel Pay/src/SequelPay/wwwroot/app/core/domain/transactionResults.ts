﻿import { IEnumEntity } from './enumEntity';

export var TransactionResults: IEnumEntity[] = [
    {
        name: 'Any',
        value: 0
    },
    {
        name: 'Approved',
        value: 1
    },
    {
        name: 'Declined',
        value: 2
    },
    {
        name: 'Error',
        value: 3
    }
];

