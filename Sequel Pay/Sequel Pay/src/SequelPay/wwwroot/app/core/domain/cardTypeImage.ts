﻿export interface ICardTypeImage {
    enumValue: number;
    src: string;
}