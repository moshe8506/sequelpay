﻿import { ICardTypeImage } from './cardTypeImage';

export var cardTypeImages64: ICardTypeImage[] = [
    {
        enumValue: 0,
        src : ''
    },
    {
        enumValue: 1,
        src: '../../lib/img/visa64.png'
    },
    {
        enumValue: 2,
        src: '../../lib/img/mc64.png'
    },
    {
        enumValue: 3,
        src: '../../lib/img/discover64.png'
    },
    {
        enumValue: 4,
        src: '../../lib/img/amex64.png'
    },
    {
        enumValue: 5,
        src: '../../lib/img/ebt64.png'
    },
    {
        enumValue: 6,
        src: '../../lib/img/ebt64.png'
    },      
    {
        enumValue: 7,
        src: '../../lib/img/ebt64.png'
    }    
]