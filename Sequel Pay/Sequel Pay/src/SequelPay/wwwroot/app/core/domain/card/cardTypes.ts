﻿export enum CardTypes {
    Any,
    Visa,
    Mastercard,
    Discover,
    Amex,
    EbtFs,
    EbtCash,
    EbtCommon
}