﻿import { ICardTypeImage } from './cardTypeImage';

export var cardTypeImages: ICardTypeImage[] = [
    {
        enumValue: 0,
        src : ''
    },
    {
        enumValue: 1,
        src: '../../lib/img/visa.png'
    },
    {
        enumValue: 2,
        src: '../../lib/img/mc.png'
    },
    {
        enumValue: 3,
        src: '../../lib/img/discover.png'
    },
    {
        enumValue: 4,
        src: '../../lib/img/amex.png'
    },
    {
        enumValue: 5,
        src: '../../lib/img/ebt.png'
    },
    {
        enumValue: 6,
        src: '../../lib/img/ebt.png'
    }
]