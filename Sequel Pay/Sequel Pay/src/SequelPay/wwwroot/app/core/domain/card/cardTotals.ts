﻿import { ICardTotal } from './cardTotal'

export interface ICardTotals {
    cards: Array<ICardTotal>;
    totalAmount: number;
    totalCount: number;
}