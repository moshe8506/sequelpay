﻿export class Registration {
    Username: string;
    Password: string;
    Email: string;
    StoreId: string;

    constructor(username: string,
        password: string,
        email: string,
        storeId: string
    ) {
        this.Username = username;
        this.Password = password;
        this.Email = email;
        this.StoreId = storeId;
    }
}