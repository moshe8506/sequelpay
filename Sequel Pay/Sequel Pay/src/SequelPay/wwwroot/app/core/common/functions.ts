﻿export class Functions {

    stringAsDate(dateStr) {
        return new Date(dateStr);
    }
    stringAsNumber(numStr) {
        return Number(numStr);
    }
}