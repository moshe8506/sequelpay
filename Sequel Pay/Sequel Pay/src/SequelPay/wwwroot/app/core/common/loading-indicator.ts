﻿import {Component} from '@angular/core';
import {Functions} from './functions';

export class LoadingPage extends Functions {
    public loading: boolean;
    constructor(val: boolean) {
        super();
        this.loading = val;
   
    }
    standby() {
        this.loading = true;
    }
    ready() {
        this.loading = false;
    }
}

@Component({
    selector: 'loading-indicator',
    template: '<img src="./lib/fonts/ring-alt.svg"/>'
    
   })
export class LoadingIndicator { }