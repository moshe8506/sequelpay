﻿import { Injectable } from '@angular/core';
import { CardTypes } from '../domain/card/cardTypes';

@Injectable()
export class CardsService {
    private cardTypes: Array<string> = [
        'Any',
        'Visa',
        'Mastercard',
        'Discover',
        'Amex',
        'Ebt Fs',
        'Ebt Cash',
        'Ebt'
    ];

    getCardNameByType(cardType: CardTypes) {
        return cardType < this.cardTypes.length ? this.cardTypes[cardType] : '';
    }

    getCardTypeByName(name: string): CardTypes {
        for (let i = 0; i < this.cardTypes.length; i++) {
            if (this.cardTypes[i].toUpperCase() === name.toUpperCase()) {
                return i;
            }
        }

        return CardTypes.Any;
    }

    getCardNames(): Array<string> {
        return this.cardTypes.filter(x => {
            var type = this.getCardTypeByName(x);
            return type !== CardTypes.EbtCash && type !== CardTypes.EbtFs;
        });
    }
}