﻿import { Http, Response, URLSearchParams  } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ITransactionFilter } from '../../components/transaction/transactionFilter';
import { ICardTotals } from '../domain/card/cardTotals';
import { SalesVolume } from '../domain/card/salesVolume';
import { SalesVolumeWeekly } from '../domain/card/salesVolumeWeekly';
import { SalesVolumeDaily } from '../domain/card/salesVolumeDaily';

@Injectable()
export class CardTotalsService {
    constructor(private http: Http) {        
    }

    getTotalByCard(transFilter: ITransactionFilter) {
        let url = 'api/Transactions/totals';
        let params = this.setCommonParams(transFilter);
        return this.http.get(url, { search: params }).map(response => (response.json() as ICardTotals));
    }

    getTotalByCardByDate(transFilter: ITransactionFilter) {
        let url = 'api/Chart';
        let params = this.setCommonParams(transFilter);
        // todo return type
        return this.http.get(url, { search: params }).map(response => response.json());
    }

    getSalesVolume(transFilter: ITransactionFilter) {
        let url = 'api/Chart/SalesVolume';
        let params = this.setCommonParams(transFilter);
        return this.http.get(url, { search: params }).map(response => response.json() as SalesVolume);
    }

    getSalesVolumeWeekly(transFilter: ITransactionFilter) {
        let url = 'api/Chart/SalesVolumeWeekly';
        let params = this.setCommonParams(transFilter);
        return this.http.get(url, { search: params }).map(response => response.json() as Array<SalesVolumeWeekly>);
    }

    getSalesVolumDaily(page : number, pageSize: number, transFilter: ITransactionFilter) {
        let url = `api/Chart/SalesVolumeDaily/${page}/${pageSize}`;
        let params = this.setCommonParams(transFilter);
        return this.http.get(url, { search: params }).map(response => response.json() as Array<SalesVolumeDaily>);
    }

    private setCommonParams(transFilter: ITransactionFilter): URLSearchParams {
        let params = new URLSearchParams();
        if (transFilter) {
            if (transFilter.dateInterval) {
                params.set('StartDate', transFilter.dateInterval.StartDate.toDateString());
                params.set('EndDate', transFilter.dateInterval.EndDate.toDateString());
            }
        }
        return params;
    }
}