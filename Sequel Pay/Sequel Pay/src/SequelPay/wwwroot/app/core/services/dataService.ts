﻿import { Http, Response, URLSearchParams  } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ITransactionFilter } from '../../components/transaction/transactionFilter';

@Injectable()
export class DataService {    
    public _baseUri: string;
    public _storeID: string;

    constructor(public http: Http) {}

    set(baseUri: string): void {
        this._baseUri = baseUri;
    }

    get(page: number, itemsPerPage: number, transFilter: ITransactionFilter ) {
        var uri = `${this._baseUri}${page}/${itemsPerPage}`;
        let params = new URLSearchParams();
        if (transFilter) {
            if (transFilter.dateInterval) {
                params.set('StartDate', transFilter.dateInterval.StartDate.toDateString());
                params.set('EndDate', transFilter.dateInterval.EndDate.toDateString());
            }
            if (transFilter.cardType != null) {
                params.set('CardType', transFilter.cardType.toString());
            }
            if (transFilter.transactionResult != null) {
                params.set('TransactionResult', transFilter.transactionResult.toString());
            }
            if (transFilter.lastFourCardDigints) {
                params.set('LastFourCardDigints', transFilter.lastFourCardDigints.toString());
            }
            if (transFilter.invoice) {
                params.set('Invoice', transFilter.invoice);
            }
            if (transFilter.customerName) {
                params.set('CustomerName', transFilter.customerName);
            }
            if (transFilter.amountFrom) {
                params.set('AmountFrom', transFilter.amountFrom.toString());
            } 
            if (transFilter.amountTo) {
                params.set('AmountTo', transFilter.amountTo.toString());
            }        
            if (transFilter.superCardNumberFilter) {
                params.set('SuperCardNumberFilter', transFilter.superCardNumberFilter);
            }   
        }

        return this.http.get(uri, { search: params })
            .map(response => response);
    }

    post(data?: any, mapJson: boolean = true) {
        if (mapJson)
            return this.http.post(this._baseUri, data)
                .map(response => <any>(<Response>response).json());
        else
            return this.http.post(this._baseUri, data);
    }

    delete(id: number) {
        return this.http.delete(this._baseUri + '/' + id.toString())
            .map(response => <any>(<Response>response).json())
    }

    deleteResource(resource: string) {
        return this.http.delete(resource)
            .map(response => <any>(<Response>response).json())
    }
}