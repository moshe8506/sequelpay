﻿import { Route, Router } from '@angular/router-deprecated';
import { Home } from './components/home';
import { Transaction } from './components/transaction/transaction';
import { Account } from './components/account/account';
import { Dashboard } from './components/dashboard/dashboard';
import { UserListComponent } from './components/account/user/user-list-component';
import { DashboardComponent } from './components/dashboardv2/dashboard.component';

export var routes = {
    home: new Route({ path: '/', name: 'Home', component: Transaction }),
    transactions: new Route({ path: '/transactions', name: 'Transactions', component: Transaction }),
    account: new Route({ path: '/account/...', name: 'Account', component: Account }),
    dashboard: new Route({ path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true }),
    userList: new Route({ path: '/users', name: 'UserList', component: UserListComponent })    
};

export const APP_ROUTES = Object.keys(routes).map(r => routes[r]);
