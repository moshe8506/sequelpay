﻿import '@angular/core';
import '@angular/common';
import '@angular/platform-browser-dynamic';
import '@angular/http';
import '@angular/router-deprecated';
import '@angular/common';
import 'rxjs/add/operator/map';
import '@angular/core';

import '../../node_modules/angular2-in-memory-web-api';

import 'ng2-bootstrap';
import 'ng2-table';
import 'moment';


import '../../node_modules/jquery/dist/jquery.js';
import '../../bower_components/bootstrap/dist/js/bootstrap.js';
import '../../node_modules/fancybox/dist/js/jquery.fancybox.pack.js';
import '../../bower_components/alertify.js/lib/alertify.min.js';
import '../../bower_components/datatables-bootstrap/1/dataTables.bootstrap.js';
import '../../systemjs.config.js';
import '../../node_modules/ng2-bootstrap/ng2-bootstrap.js';
import '../../node_modules/ng2-table/ng2-table.js';        
import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../node_modules/fancybox/dist/css/jquery.fancybox.css';
import '../../bower_components/components-font-awesome/css/font-awesome.css';
import '../../bower_components/alertify.js/themes/alertify.core.css';
import '../../bower_components/alertify.js/themes/alertify.bootstrap.css';
import '../../bower_components/alertify.js/themes/alertify.default.css';
import '../../bower_components/datatables-bootstrap/1/dataTables.bootstrap.css';
import '../css/bootstrap.min.css';
import '../css/animate.css';

import '../../bower_components/datatables-tabletools/swf/copy_csv_xls_pdf.swf';


import '../../node_modules/fancybox/dist/img/blank.gif';
import '../../node_modules/fancybox/dist/img/fancybox_loading.gif';
import '../../node_modules/fancybox/dist/img/fancybox_loading@2x.gif';
import '../../node_modules/fancybox/dist/img/fancybox_overlay.png';
import '../../node_modules/fancybox/dist/img/fancybox_sprite.png';
import '../../node_modules/fancybox/dist/img/fancybox_sprite@2x.png';
import '../images/payment/amex.png';
import '../images/payment/discover.png';
import '../images/payment/mc.png';
import '../images/payment/visa.png';
import '../images/logo/logo.png';
import '../images/payment/ebt.png';



import '../../node_modules/bootstrap/fonts/glyphicons-halflings-regular.eot';
import '../../node_modules/bootstrap/fonts/glyphicons-halflings-regular.svg';
import '../../node_modules/bootstrap/fonts/glyphicons-halflings-regular.ttf';
import '../../node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff';
import '../../node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff2';
import '../../bower_components/components-font-awesome/fonts/FontAwesome.otf';
import '../../bower_components/components-font-awesome/fonts/fontawesome-webfont.eot';
import '../../bower_components/components-font-awesome/fonts/fontawesome-webfont.svg';
import '../../bower_components/components-font-awesome/fonts/fontawesome-webfont.ttf';
import '../../bower_components/components-font-awesome/fonts/fontawesome-webfont.woff';
import '../../bower_components/components-font-awesome/fonts/fontawesome-webfont.woff2';
import '../fonts/ring-alt.svg';