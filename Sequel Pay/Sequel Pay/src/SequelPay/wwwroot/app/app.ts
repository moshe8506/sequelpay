﻿///<reference path="../../node_modules/angular2-in-memory-web-api/typings/browser.d.ts" /> 

import {provide, Component, OnInit } from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {HTTP_BINDINGS, HTTP_PROVIDERS, Headers, RequestOptions, BaseRequestOptions} from '@angular/http';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import 'rxjs/add/operator/map';
import {enableProdMode} from '@angular/core';

//import 'rxjs/Rx';   // Load all features

enableProdMode();
import { routes, APP_ROUTES } from './routes';

import { DataService } from './core/services/dataService';
import { MembershipService } from './core/services/membershipService';
import { UtilityService } from './core/services/utilityService';
import { CardTotalsService } from './core/services/card.totals.service';
import { CardsService } from './core/services/cards.service';
import { User } from './core/domain/user';

@Component({
    selector: 'sequelpay-app',
    templateUrl: './app/app.html',
    directives: [ROUTER_DIRECTIVES, CORE_DIRECTIVES]
})
@RouteConfig(APP_ROUTES)
export class AppRoot implements OnInit {
    private routes = routes;

    constructor(public membershipService: MembershipService,
                public utilityService: UtilityService,
                public location: Location,
                private router: Router
    ) { }

    ngOnInit() {        
        this.router.navigate([localStorage.getItem('returnURL')]);
    }

    isUserLoggedIn(): boolean {
        return this.membershipService.isUserAuthenticated();
    }

    getUserName(): string {
        if (this.isUserLoggedIn()) {
            var _user = this.membershipService.getLoggedInUser();
            return _user.Username;
        }
        else
            return 'Merchant Login';
    }

    logout(): void {
         this.membershipService.logout()
            .subscribe(res => {
            localStorage.removeItem('user');
            this.utilityService.navigateToSignIn();  
          
            },
            error => console.error('Error: ' + error),
            () => { });
    }
}

class AppBaseRequestOptions extends BaseRequestOptions {
    headers: Headers = new Headers({
        'Content-Type': 'application/json'
    })
}

bootstrap(AppRoot, [HTTP_PROVIDERS, ROUTER_PROVIDERS,
    provide(RequestOptions, { useClass: AppBaseRequestOptions }),
    provide(LocationStrategy, { useClass: HashLocationStrategy }),
    DataService, MembershipService, UtilityService, CardTotalsService, CardsService])
    .catch(err => console.error(err));

// ROUTER_BINDINGS: DO NOT USE HERE IF YOU WANT TO HAVE HASHLOCATIONSTRATEGY!!