﻿import {Component} from '@angular/core';


import {CORE_DIRECTIVES, FORM_DIRECTIVES} from '@angular/common';
import {Router, RouterLink} from '@angular/router-deprecated'
import { Transactions } from '../../core/domain/transaction';
import { Paginated } from '../../core/common/paginated';
import { DataService } from '../../core/services/dataService';
import { UtilityService } from '../../core/services/utilityService';
import { NotificationService } from '../../core/services/notificationService';
import { routes as Routes, APP_ROUTES } from '../../routes';
import { MembershipService } from '../../core/services/membershipService';
import {LoadingIndicator, LoadingPage} from '../../core/common/loading-indicator';


@Component({
    selector: 'dashboard',
    templateUrl: './app/components/dashboard/dashboard.html',

    providers: [NotificationService],

    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, RouterLink]
})
export class Dashboard extends Paginated {
    private _tranAPI: string = 'api/Transactions/';
    private _trans: Array<Dashboard>;
    private routes = Routes;
    private cols;//: Array<string>;

    constructor(public tranService: DataService,
        public utilityService: UtilityService,
        public notificationService: NotificationService,
        public membershipService: MembershipService,
        public router: Router) {
        super(0, 0, 0);


        this.routes = Routes;
        tranService.set(this._tranAPI);
        localStorage.setItem('returnURL', Routes.dashboard.name);

        this.cols = [
            { field: "Id", header: "ID" },
            { field: "CardType", header: "Card Type" },
            { field: "DateCreated", header: "Transaction Date" },
            { field: "Amount", header: "Amount" },
            { field: "NameOnCard", header: "Customer" },
           { field: "CardNumber", header: "Card Number" },
           { field: "RefNumber", header: "Auth #" },
           { field: "TranNumber", header: "Invoice" }
        ];

    }

    isUserLoggedIn(): boolean {
        return this.membershipService.isUserAuthenticated();
    }
    getUserName(): string {
        if (this.isUserLoggedIn()) {
            var _user = this.membershipService.getLoggedInUser();
            return _user.Username;
        }
        else
            return 'Merchant Login';
    }
}