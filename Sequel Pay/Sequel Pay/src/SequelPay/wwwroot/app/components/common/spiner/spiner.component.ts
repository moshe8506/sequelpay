﻿import { Component, Input } from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from '@angular/common';

@Component({
    selector: 'sp-spiner',
    templateUrl: './app/components/common/spiner/spiner.component.html'
})
export class SpinerComponent {
    @Input()
    private loading: boolean;

    constructor() {}    
}