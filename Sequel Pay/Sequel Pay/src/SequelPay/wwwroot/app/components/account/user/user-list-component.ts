﻿import {Component, OnInit} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from '@angular/common';
import { MembershipService } from '../../../core/services/membershipService';
import { NotificationService } from '../../../core/services/notificationService';
import { Registration } from '../../../core/domain/registration';
import { OperationResult } from '../../../core/domain/operationResult';

@Component({
    selector: 'user-list',
    templateUrl: './app/components/account/user/user-list-component.html',
    providers: [MembershipService, NotificationService],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class UserListComponent implements OnInit {
    public userName: string;
    public email: string;
    public password: string;
    public storeId: string;

    public constructor(
        public membershipService: MembershipService,
        public notificationService: NotificationService
    ) { }

    ngOnInit() {        
    }

    register() {
        var registration = new Registration(this.userName, this.password, this.email, this.storeId);
        var operationResult = new OperationResult(false, '');
        this.membershipService.register(registration).subscribe((result) => {
            operationResult.Succeeded = result.Succeeded;
            operationResult.Message = result.Message;
        }, error => {
            console.error('Error: ' + error);
        }, () => {
            if (operationResult.Succeeded) {
                this.notificationService.printSuccessMessage(`${registration.Username} successfully registeresd`);
            }
            else {
                this.notificationService.printErrorMessage(operationResult.Message);
            }
        });
    }
}