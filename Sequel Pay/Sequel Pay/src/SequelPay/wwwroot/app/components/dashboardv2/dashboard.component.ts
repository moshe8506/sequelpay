﻿import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from '@angular/common';
import {Router, RouterLink} from '@angular/router-deprecated';

import { CardTotalsService } from '../../core/services/card.totals.service';

import { ITransactionFilter } from '../transaction/transactionFilter';
import { IDateFilter } from '../transaction/dateFilter';

import { ICardTotals } from '../../core/domain/card/cardTotals';
import { ICardTotal } from '../../core/domain/card/cardTotal';
import { CardTypes } from '../../core/domain/card/cardTypes';
import { SalesVolume } from '../../core/domain/card/salesVolume';
import { SalesVolumeWeekly } from '../../core/domain/card/salesVolumeWeekly';

import { TransactionResults } from '../../core/domain/transactionResults';
import { SpinerComponent } from '../common/spiner/spiner.component';


import * as moment from 'moment';
declare var Morris: any;

@Component({
    templateUrl: './app/components/dashboardv2/dashboard.component.html',
    selector: 'dashboardv2',
    directives: [SpinerComponent],
    providers: [CardTotalsService]
})
export class DashboardComponent implements OnInit, AfterViewInit {
    @ViewChild('chartActiveButton') chartActiveButton: ElementRef;
    @ViewChild('salesVolumeActiveButton') salesVolumeActiveButton: ElementRef;
    @ViewChild('salesVolumeWeeklyActiveButton') salesVolumeWeeklyActiveButton: ElementRef;

    private cardTotals: ICardTotal[];    

    private today: Date;    
    private chart: any;

    private chartActiveButtonElement: any;
    private salesVolumeActiveButtonElement: any;
    private salesVolumeWeeklyActiveButtonElement: any;

    private loading: boolean;
    private loadingSalesVolume: boolean;
    private loadingSalesVolumeWeekly: boolean;
    private cardWidgetColors: Array<string>;
    private salesVolume: SalesVolume;
    private salesVolumeWeekly: Array<SalesVolumeWeekly>;


    constructor(private cardTotalsService: CardTotalsService) {
        this.today = moment().toDate();
        this.loading = false;
        this.cardWidgetColors = ['blue-bg', 'navy-bg', 'purple-bg', 'yellow-bg', 'red-bg', 'lazur-bg'];
    }

    ngOnInit() {
        let dateFilter: IDateFilter = {
            StartDate: this.today,
            EndDate: this.today
        };

        this.cardTotalsService.getTotalByCard(this.createFilter(dateFilter))
            .subscribe(res => {
                    this.cardTotals = res.cards.filter(c => {
                        return c.CardType !== CardTypes.Any;
                });
                    this.cardTotals.push({
                        CardType: CardTypes.Any,
                        Name: 'Total',
                        Count: res.totalCount,
                        Amount: res.totalAmount
                    });                    
                },
                error => {
                    // todo error handler
                });
    }

    ngAfterViewInit() {    
        this.chartActiveButtonElement = this.chartActiveButton.nativeElement;
        this.salesVolumeActiveButtonElement = this.salesVolumeActiveButton.nativeElement;
        this.salesVolumeWeeklyActiveButtonElement = this.salesVolumeWeeklyActiveButton.nativeElement;

        let dateFilter: IDateFilter = {
            StartDate: moment().subtract(30, "days").toDate(),
            EndDate: moment().toDate()
        };
        this.loadDataChart(dateFilter);
        this.loadSalesVolume(dateFilter);
        this.loadSalesVolumeWeekly(dateFilter);
        this.loadSalesVolumeDaily(dateFilter);
    }

    private createFilter(dateFilter: IDateFilter): ITransactionFilter {
        return {
            cardType: undefined,
            dateInterval: dateFilter,
            transactionResult: TransactionResults[1].value,
            lastFourCardDigints: undefined,
            invoice: undefined,
            customerName: undefined,
            amountFrom: undefined,
            amountTo: undefined,
            superCardNumberFilter: undefined
        };
    }

    // todo check data on EbtFs
    loadChart(data: Array<any>) {
       for (let item of data) {
           item.DateCreatedFormated = moment(item.DateCreated).format("YYYY-MM-DD");
       }
        if (this.chart) {
            this.chart.setData(data);
        } else {
            this.chart = Morris.Area({
                element: 'morris-area-chart',
                data: data,
                xkey: 'DateCreatedFormated',
                ykeys: ['Amex', 'Discover', 'EbtCommon', 'Mastercard', 'Visa'],
                labels: ['Amex', 'Discover', 'Ebt', 'Mastercard', 'Visa'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true,
                lineColors: ['#a94442', '#23c6c8', '#f8ac59', '#1c84c6', '#f99295'],
                lineWidth: 2
            });
        }
    }

    loadDataChart(dateFilter: IDateFilter) {
        this.loading = true;
        var filter = this.createFilter(dateFilter);
        this.cardTotalsService.getTotalByCardByDate(filter)
            .subscribe(result => {
                    this.loadChart(result);
                    this.loading = false;
                },
                error => {
                    // todo error handler
                });
    }

    chartDateClick($event, count, type) {
        let dateFilter = this.changeDateActiveButton(this.chartActiveButtonElement, $event, count, type);
        this.chartActiveButtonElement = $event.target;
        this.loadDataChart(dateFilter); 
    }

    salesVolumeDateClick($event, count, type) {
        let dateFilter = this.changeDateActiveButton(this.salesVolumeActiveButtonElement, $event, count, type);
        this.salesVolumeActiveButtonElement = $event.target;
        this.loadSalesVolume(dateFilter);
    }

    salesVolumeWeeklyDateClick($event, count, type) {
        let dateFilter = this.changeDateActiveButton(this.salesVolumeWeeklyActiveButtonElement, $event, count, type);
        this.salesVolumeWeeklyActiveButtonElement = $event.target;
        this.loadSalesVolumeWeekly(dateFilter);
    }

    changeDateActiveButton(activeButtonElement, $event, count, type) : IDateFilter {
        activeButtonElement.classList.remove('btn-primary');
        activeButtonElement.className += ' btn-default';

        $event.target.classList.remove('btn-default');
        $event.target.className += ' btn-primary';        

        return {
            StartDate: moment().subtract(count, type).toDate(),
            EndDate: moment().toDate()
        };        
    }

    getCardWidgetClass(num) {
        if (num >= 0 && num < this.cardWidgetColors.length) {
            return this.cardWidgetColors[num];
        }
        return '';
    }

    loadSalesVolume(dateFilter: IDateFilter) {
        this.loadingSalesVolume = true;
        var filter = this.createFilter(dateFilter);
        this.cardTotalsService.getSalesVolume(filter)
            .subscribe(result => {
                    this.salesVolume = result;
                    this.loadingSalesVolume = false;
                },
                error => {
                    // todo error handler
                }
            );
    }

    loadSalesVolumeWeekly(dateFilter: IDateFilter) {
        this.loadingSalesVolumeWeekly = true;
        var filter = this.createFilter(dateFilter);
        this.cardTotalsService.getSalesVolumeWeekly(filter)
            .subscribe(result => {
                    this.salesVolumeWeekly = result;
                    this.loadingSalesVolumeWeekly = false;
                },
                error => {
                    // todo error handler
                }
            );
    }

    loadSalesVolumeDaily(dateFilter: IDateFilter) {
        //this.loadingSalesVolumeWeekly = true;
        var filter = this.createFilter(dateFilter);
        this.cardTotalsService.getSalesVolumDaily(0,10,filter)
            .subscribe(result => {
                    debugger;
                    //this.loadingSalesVolumeWeekly = false;
                },
            error => {
                // todo error handler
            }
            );
    }
}

