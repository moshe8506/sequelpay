﻿import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, Control, FormBuilder} from '@angular/common';
import {Router, RouterLink} from '@angular/router-deprecated'
import { Transactions } from '../../core/domain/transaction';
import { CardTypes } from '../../core/domain/card/cardTypes';
import { TransactionResults } from '../../core/domain/transactionResults';
import { IEnumEntity } from '../../core/domain/enumEntity';
import { ICardTypeImage } from '../../core/domain/cardTypeImage';
import { cardTypeImages } from '../../core/domain/cardTypeImages';
import { cardTypeImages64 } from '../../core/domain/cardTypeImages64';
import { Paginated } from '../../core/common/paginated';
import { DataService } from '../../core/services/dataService';
import { UtilityService } from '../../core/services/utilityService';
import { CardsService } from '../../core/services/cards.service';
import { NotificationService } from '../../core/services/notificationService';
import { routes as Routes, APP_ROUTES } from '../../routes';
import { MembershipService } from '../../core/services/membershipService';
import {LoadingIndicator} from '../../core/common/loading-indicator';

import { IDateFilter } from './dateFilter';
import { ITransactionFilter } from './transactionFilter';
import { IValidationResult } from '../../core/common/validationResult';

import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {NG_TABLE_DIRECTIVES} from 'ng2-table/ng2-table';

import { ContextMenuComponent, ContextMenuService } from 'angular2-contextmenu/angular2-contextmenu';


declare var jQuery: any;
declare var moment: any;

@Component({
    selector: 'transaction',
    providers: [NotificationService, ContextMenuService],
    templateUrl: './app/components/transaction/transaction.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, NG_TABLE_DIRECTIVES, PAGINATION_DIRECTIVES, RouterLink, LoadingIndicator, ContextMenuComponent ]
})
export class Transaction extends Paginated implements OnInit, AfterViewInit  {
    @ViewChild('dateFilter') dateFilter: ElementRef;
    @ViewChild('cardTypeFilter') cardTypeFilter: ElementRef;
    @ViewChild('tranResultFilter') tranResultFilter: ElementRef;
    @ViewChild('cardNumberFilter') cardNumberFilter: ElementRef;    

    private _tranAPI: string = 'api/Transactions/';
    private _trans: any[];

    // todo class
    private cardTotals: any[];

    private routes = Routes;
    private cardTypes = CardTypes;
    private transactionResults = TransactionResults;    

    // filter
    private selectedCardType: number;
    private transactionResult: number;
    private lastFourDigitsCard: number;
    private invoiceFilter: string;
    private customerName: string;
    private amountFrom: number;
    private amountTo: number;
    private superCardNumberFilter: string;
    private filterForm: any;

    // pagination
    public page: number;
    public itemsPerPage: number;
    public maxSize: number;
    public numPages: number;
    public length: number;
    public config: any = {
        paging: true        
    };        

    //total
    public totalByAllCards : number;

    // caption
    public startDateCaption: Date;
    public endDateCaption: Date;
    public isShowForAllDates : boolean;

    constructor(public tranService: DataService,
        public utilityService: UtilityService,
        public notificationService: NotificationService,
        public router: Router,
        public membershipService: MembershipService,
        private formBuilder: FormBuilder,
        private contextMenuService: ContextMenuService,
        private cardsService: CardsService
    ) {                
        super(0, 0, 0);
        // todo move to service
        localStorage.setItem('returnURL', Routes.transactions.name);
        this.routes = Routes;
        tranService.set(this._tranAPI);
        
        this.selectedCardType = CardTypes.Any;
        // approved as a default value
        this.transactionResult = this.transactionResults[1].value;

        this.filterForm = this.formBuilder.group({
            cardNumber: ['', this.validateCardNumberInput]
        });

        this.setPaginationDefaultValues();        
    }

    ngOnInit() {
        if (!this.isUserLoggedIn()) {
            localStorage.setItem('returnURL', Routes.transactions.name);
            this.utilityService.navigateToSignIn();
        } else {
            this.onChangeTable(this.config);
        }
    }

    ngAfterViewInit() {
        this.initDateRangePicker();
    }

    private initDateRangePicker(): void {
        var me = this;

        var cb = (start, end) => {
            jQuery('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        cb(moment(), moment());

        jQuery('#reportrange').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        jQuery('#reportrange').on('apply.daterangepicker', (ev, picker)=> {
            me.refresh();
        });
    }

    public onContextMenu($event: MouseEvent, trans: any): void {
        this.contextMenuService.show.next({
            actions: [
                {
                    html: () => `Find all by this card`,
                    click: (item) => {
                        this.findBySpecificCard(item.CardNumber);
                    }
                }                
            ],
            event: $event,
            item: trans
        });
        $event.preventDefault();
    }

    getTransactions(): void {
        this.updateCaptionDates();
        this.standby();
        this.tranService.get(this.page - 1, this.itemsPerPage, this.getFilterData())
            .subscribe(res => {
                    var data: any = res.json();
                    this._trans = data.Items;
                    // todo type for TotalCards
                    this.cardTotals = data.TotalCards.filter((i: any) => {
                        return i.CardType != CardTypes.Any;
                    });
                    this.setCardTypeImgsSrc(this._trans);
                    this.totalByAllCards = data.TotalByAllCards;
                    this.length = data.TotalCount;
                    this.ready();
                },
                error => {
                    this.ready();
                    if (error.status == 401) {
                        this.notificationService.printErrorMessage('Authentication required');
                        localStorage.setItem('returnURL', Routes.transactions.name);
                        this.utilityService.navigateToSignIn();
                    }
                });
    }

   isUserLoggedIn(): boolean {
        return this.membershipService.isUserAuthenticated();
    }
    getUserName(): string {
        if (this.isUserLoggedIn()) {
            var _user = this.membershipService.getLoggedInUser();
            return _user.Username;
        }
        else
            return 'Merchant Login';
   }

    private getDateFilterData(): IDateFilter {
        let startDate = moment();
        let endDate = moment();
        if (this.dateFilter) {
            let dateFilterData = jQuery(this.dateFilter.nativeElement).data('daterangepicker');
            startDate = dateFilterData.startDate;
            endDate = dateFilterData.endDate;
        }

        return {
            StartDate: startDate.toDate(),
            EndDate: endDate.toDate()
        };
    }

    private getFilterData(): ITransactionFilter {
        var superCardFilter = this.superCardNumberFilter;
        this.superCardNumberFilter = undefined;        

        return {
            cardType: this.selectedCardType,
            dateInterval: this.getDateFilterData(),
            transactionResult: this.transactionResult,
            lastFourCardDigints: this.lastFourDigitsCard,
            invoice: this.invoiceFilter,
            customerName: this.customerName,
            amountFrom: this.amountFrom,
            amountTo: this.amountTo,
            superCardNumberFilter: superCardFilter
        }        
    }

    private validateCardNumberInput(control: any): IValidationResult {
        if (control.value && !control.value.match(/^\d{4}$/)) {
            return { "incorrectCardNumverFormat": true };   
        }
        return null;
    }

    // todo service to get card types names
    private setCardTypeImgsSrc(transactions: any[]): void {
        transactions.forEach((transaction: any) => {   
            let cardType  : CardTypes = this.cardsService.getCardTypeByName(transaction.CardType);
            if (cardType) {
                transaction.cardImgSrc = this.getCardTypeImgSrc(cardType, cardTypeImages);
            }
        });        
    }

    private getCardTypeImgSrc(cardType: number, images: Array<ICardTypeImage>): string {
        let imgSrc = images.filter((i) => {
            return i.enumValue === cardType;
        });

        return imgSrc.length ? imgSrc[0].src : undefined;
    }

    private getCardTypeImg64Src(cardType: number): string {
        return this.getCardTypeImgSrc(cardType, cardTypeImages64);
    }

    private setPaginationDefaultValues() {
        this.page = 1;
        this.itemsPerPage = 50;
        this.maxSize = 5;
        this.numPages = 1;
        this.length = 0;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        this.page = page.page;
        this.getTransactions();
    }

    public refresh() {
        this.isShowForAllDates = false;
        this.refreshDataInTable();
    }

    private refreshDataInTable() {
        this.setPaginationDefaultValues();
        this.onChangeTable(this.config);        
    }

    private updateCaptionDates(): void {
        var dateFilterData = this.getDateFilterData();
        this.startDateCaption = dateFilterData.StartDate;
        this.endDateCaption = dateFilterData.EndDate;        
    }

    public findBySpecificCard(cardNumber) {
        this.isShowForAllDates = true;
        this.superCardNumberFilter = cardNumber;
        this.refreshDataInTable();;
    }
}

