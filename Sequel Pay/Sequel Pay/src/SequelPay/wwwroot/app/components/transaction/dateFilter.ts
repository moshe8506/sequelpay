﻿export interface IDateFilter {
    StartDate: Date;
    EndDate: Date;
}