﻿import { IDateFilter } from './dateFilter';
import { IEnumEntity } from '../../core/domain/enumEntity';

export interface ITransactionFilter {
    dateInterval: IDateFilter;
    cardType: number;
    transactionResult: number;
    lastFourCardDigints: number;
    invoice: string;
    customerName: string;
    amountFrom: number;
    amountTo: number;
    superCardNumberFilter: string;
}