/// <binding BeforeBuild='build-spa' />
var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    merge = require('merge'),
    fs = require("fs"),
    del = require('del'),
    path = require('path');

var sourcemaps = require('gulp-sourcemaps');

eval("var project = " + fs.readFileSync("./project.json"));
var lib = "./" + project.webroot + "/lib/";
var inspiniaLib = '/inspinia';

var paths = {
    npm: './node_modules/',
    tsSource: './wwwroot/app/**/*.ts',
    tsOutput: lib + 'spa/',
    tsDef: lib + 'definitions/',

    // js
    jsVendors: lib + 'js',
    jsVendorsInspinia: lib + 'js' + inspiniaLib,

    jsRxJSVendors: lib + 'js/rxjs',

    // css
    cssVendors: lib + 'css',
    cssVendorsInspinia: lib + 'css' + inspiniaLib,

    // img
    imgVendors: lib + 'img',

    // fonts
    fontsVendors: lib + 'fonts',
    fontsVendorsInspinia: lib + 'fonts' + inspiniaLib
};


var tsProject = ts.createProject('./tsconfig.json');

gulp.task('setup-vendors', function (done) {
    gulp.src([
      'node_modules/jquery/dist/jquery.*js',
      'bower_components/bootstrap/dist/js/bootstrap*.js',
      'node_modules/fancybox/dist/js/jquery.fancybox.pack.js',
      'bower_components/alertify.js/lib/alertify.min.js',
      'bower_components/datatables-bootstrap/1/dataTables.bootstrap.js',
      'systemjs.config.js',
      'node_modules/ng2-bootstrap/ng2-bootstrap.js',
      'node_modules/ng2-table/ng2-table.js',
      'node_modules/angular2-contextmenu/angular2-contextmenu.js'
    ]).pipe(gulp.dest(paths.jsVendors));

    gulp.src([
      'bower_components/bootstrap/dist/css/bootstrap.css',
      'node_modules/fancybox/dist/css/jquery.fancybox.css',
      'bower_components/components-font-awesome/css/font-awesome.css',
      'bower_components/alertify.js/themes/alertify.core.css',
      'bower_components/alertify.js/themes/alertify.bootstrap.css',
      'bower_components/alertify.js/themes/alertify.default.css',
      'bower_components/datatables-bootstrap/1/dataTables.bootstrap.css',
      'wwwroot/css/bootstrap.min.css',
      'wwwroot/css/animate.css',
      'wwwroot/css/style.css',
      'bower_components/datatables-tabletools/swf/copy_csv_xls_pdf.swf',
      'wwwroot/css/inspinia'
    ]).pipe(gulp.dest(paths.cssVendors));

    gulp.src([
      'node_modules/fancybox/dist/img/blank.gif',
      'node_modules/fancybox/dist/img/fancybox_loading.gif',
      'node_modules/fancybox/dist/img/fancybox_loading@2x.gif',
      'node_modules/fancybox/dist/img/fancybox_overlay.png',
      'node_modules/fancybox/dist/img/fancybox_sprite.png',
      'node_modules/fancybox/dist/img/fancybox_sprite@2x.png',
      'wwwroot/images/payment/amex.png',
      'wwwroot/images/payment/discover.png',
      'wwwroot/images/payment/mc.png',
      'wwwroot/images/payment/visa.png',      
      'wwwroot/images/payment/ebt.png',
      'wwwroot/images/payment/64/amex64.png',
      'wwwroot/images/payment/64/discover64.png',
      'wwwroot/images/payment/64/mc64.png',
      'wwwroot/images/payment/64/visa64.png',
      'wwwroot/images/payment/64/ebt64.png',
      'wwwroot/images/logo/logo.png'
    ]).pipe(gulp.dest(paths.imgVendors));

    gulp.src([
      'node_modules/bootstrap/fonts/glyphicons-halflings-regular.eot',
      'node_modules/bootstrap/fonts/glyphicons-halflings-regular.svg',
      'node_modules/bootstrap/fonts/glyphicons-halflings-regular.ttf',
      'node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff',
      'node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff2',
      'bower_components/components-font-awesome/fonts/FontAwesome.otf',
      'bower_components/components-font-awesome/fonts/fontawesome-webfont.eot',
      'bower_components/components-font-awesome/fonts/fontawesome-webfont.svg',
      'bower_components/components-font-awesome/fonts/fontawesome-webfont.ttf',
      'bower_components/components-font-awesome/fonts/fontawesome-webfont.woff',
      'bower_components/components-font-awesome/fonts/fontawesome-webfont.woff2',
      'wwwroot/fonts/ring-alt.svg'
    ]).pipe(gulp.dest(paths.fontsVendors));


    // inspinia
    gulp.src([
            'wwwroot/inspinia/js/morris/morris.js',
            'wwwroot/inspinia/js/morris/raphael-2.1.0.min.js',
            'wwwroot/inspinia/js/inspinia.js',
            'wwwroot/inspinia/js/pace.min.js'
        ])
        .pipe(gulp.dest(paths.jsVendorsInspinia));

    gulp.src([
            'wwwroot/inspinia/css/animate.css',
            'wwwroot/inspinia/css/style.css',
            'wwwroot/inspinia/css/morris/morris-0.4.3.min.css'
        ])
        .pipe(gulp.dest(paths.cssVendorsInspinia));

    gulp.src([
            'wwwroot/inspinia/fonts/glyphicons-halflings-regular.eot',
            'wwwroot/inspinia/fonts/glyphicons-halflings-regular.svg',
            'wwwroot/inspinia/fonts/glyphicons-halflings-regular.ttf',
            'wwwroot/inspinia/fonts/glyphicons-halflings-regular.woff',
            'wwwroot/inspinia/fonts/glyphicons-halflings-regular.woff2'
        ])
        .pipe(gulp.dest(paths.fontsVendorsInspinia));
});

gulp.task('compile-typescript', function (done) {
    var tsResult = gulp.src([
       "wwwroot/app/**/*.ts"
    ])    
     .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());

    gulp.src("wwwroot/app/**/*.html").pipe(gulp.dest(paths.tsOutput));
    //gulp.src("wwwroot/app/**/*.map").pipe(gulp.dest(paths.tsOutput));
    //gulp.src("wwwroot/app/**/*.ts").pipe(gulp.dest(paths.tsOutput));

    return tsResult.js.pipe(gulp.dest(paths.tsOutput));
});

gulp.task('watch.ts', ['compile-typescript'], function () {
    return gulp.watch('wwwroot/app/**/*.ts', ['compile-typescript']);
});

gulp.task('watch', ['watch.ts']);

gulp.task('clean-lib', function () {
    return del([lib]);
});

gulp.task('build-spa', ['setup-vendors', 'compile-typescript']);